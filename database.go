package sql

import (
	"database/sql"
	"fmt"
	"reflect"
	"errors"
	"strings"
)

type Result sql.Result

// Database - Represents a generic database agent
type DB struct {
	*sql.DB
	Driver   string
	Host     string
	Port     uint16
	Name     string
	Username string
	Password string
}

func (db *DB) Open() (err error) {

	db.DB, err = sql.Open(db.Driver, db.connectionString())

	return
}

func (db *DB) Get(destination interface{}, query string, args...interface{}) (err error) {

	var rows *Rows

	rows, err = db.Query(query, args...)

	if err != nil {
		return
	}

	rows.Next()

	err = rows.Scan(destination)

	return
}

func (db *DB) Select(destination interface{}, query string, args...interface{}) (err error) {

	arrPtr := reflect.ValueOf(destination)
	
	if arrPtr.Kind() != reflect.Ptr || arrPtr.Elem().Kind() != reflect.Slice {
		err = errors.New("destination must be a pointer of a slice")
		return
	}
	
	var rows *Rows

	rows, err = db.Query(query, args...)

	if err != nil {
		return
	}

	for rows.Next() {

		tupleObject := reflect.New(arrPtr.Elem().Type().Elem())

		err = rows.Scan(tupleObject.Interface())

		if err != nil {
			break
		}

		arrPtr.Elem().Set(reflect.Append(arrPtr.Elem(), tupleObject.Elem()))
	}

	return
}

func (db *DB) Insert(tableName string, object interface{}) (result Result, err error) {

	columns := make([]string, 0)
	args := make([]interface{}, 0)
	fields := struct2Map(object)

	for columnName, columnValue := range fields {
		columns = append(columns, columnName)
		args = append(args, columnValue)
	}

	insertSQL := fmt.Sprintf("INSERT INTO %s(%s) VALUES (%s);", tableName, strings.Join(columns, ","), db.paramTokens(len(columns)))

	result, err = db.Exec(insertSQL, args...)

	return
}

func (db *DB) Update(tableName string, object interface{}, onlyColumns []string, where string, whereArgs...interface{}) (result Result, err error) {

	var updateQuery string

	columns := make([]string, 0)
	args := make([]interface{}, 0)
	fields := struct2Map(object)

	only := strings.Join(onlyColumns, ",")

	for columnName, columnValue := range fields {

		if (onlyColumns == nil || strings.Contains(only, columnName)) {
			
		}

		columns = append(columns, fmt.Sprintf("%s = %s", columnName, db.paramTokens(1)))
		args = append(args, columnValue)
	}

	if (len(where) > 0) {
		updateQuery = fmt.Sprintf("UPDATE %s SET %s WHERE %s", tableName, strings.Join(columns, ","), where)
		args = append(args, whereArgs...)
	} else {
		updateQuery = fmt.Sprintf("UPDATE %s SET %s", tableName, strings.Join(columns, ","))
	}

	result, err = db.Exec(updateQuery, args...)

	return
}

func (db *DB) Delete(tableName string, where string, args...interface{}) (result Result, err error) {

	var deleteQuery string

	if len(where) > 0 {
		deleteQuery = fmt.Sprintf("DELETE FROM %s WHERE %s", tableName, where)
	} else {
		deleteQuery = fmt.Sprintf("DELETE FROM %s", tableName)
	}

	result, err = db.Exec(deleteQuery, args)

	return
}

func (db *DB) Count(tableName string, where string, args...interface{}) (count int64, err error) {

	var countQuery string
	var rows *Rows

	if len(where) > 0 {
		countQuery = fmt.Sprintf("SELECT count(*) FROM %s WHERE %s", tableName, where)
	} else {
		countQuery = fmt.Sprintf("SELECT count(*) FROM %s", tableName)
	}

	rows, err = db.Query(countQuery, args...)

	if err != nil {
		return
	}

	if rows.Next() {
		err = rows.Rows.Scan(&count)
	}

	return
}

func (db *DB) Query(query string, args...interface{}) (rows *Rows, err error) {

	var sqlRow *sql.Rows

	sqlRow, err = db.DB.Query(query, args...)

	rows = &Rows{sqlRow}

	return
}

/*
 * Unexported Methods
 */

func (db *DB) connectionString() string {
	switch db.Driver {
	case "postgresql":
		return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", db.Host, db.Port, db.Username, db.Password, db.Name)
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Username, db.Password, db.Host, db.Port, db.Name)
	case "sqlite3":
		return fmt.Sprintf("file:%s", db.Name)
	default:
		panic(fmt.Errorf("\"%s\" driver is not supported", db.Driver))
	}
}

func (db *DB) paramTokens(count int) string {

	tokens := make([]string, 0)

	switch db.Driver {
	case "postgresql":
		
		for i := 1; i <= count; i++ {
			tokens = append(tokens, fmt.Sprintf("$%d", i))
		}

	default:

		for i := 1; i <= count; i++ {
			tokens = append(tokens, "?")
		}

	}

	return strings.Join(tokens, ",")
}

func struct2Map(object interface{}) map[string]interface{} {

	var val reflect.Value

	result := make(map[string]interface{})

	val = reflect.ValueOf(object)

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	typ := val.Type()

	for i := 0; i < typ.NumField(); i++ {

		field := typ.Field(i)
		
		columnName := field.Tag.Get("db")

		if len(columnName) == 0 {
			continue
		}

		columnValue := val.FieldByName(field.Name)

		if !columnValue.IsValid() {
			continue
		}

		result[columnName] = columnValue.Interface()
	}

	return result
}