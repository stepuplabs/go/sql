package sql

import (
	"database/sql"
	"reflect"
	"time"
	"fmt"
	"errors"
)

type Rows struct {
	*sql.Rows
}

func (r *Rows) Scan(destination interface{}) (err error) {

	var columns []string

	columns, err = r.Columns()

	if err != nil {
		return
	}

	scannedValues := make([]interface{}, len(columns))
	refValues := make([]interface{}, len(columns))

	for i := 0; i < len(columns); i++ {
		refValues[i] = &scannedValues[i]
	}

	err = r.Rows.Scan(refValues...)

	if err != nil {
		return
	}

	idx := 0
	values := make(map[string]interface{})

	for _, column := range columns {
		values[column] = scannedValues[idx]
		idx++
	}

	setObjectAttributes(destination, values)

	return
}

func setObjectAttributes(object interface{}, values map[string]interface{}) (err error) {
	
	val := reflect.ValueOf(object)

	if val.Kind() != reflect.Ptr {
		err = fmt.Errorf("object is not a pointer")
		return
	}

	val = val.Elem()
	typ := val.Type()

	for i := 0; i < typ.NumField(); i++ {

		fspec := typ.Field(i)
		field := val.Field(i)

		refname := fspec.Tag.Get("db")

		if len(refname) == 0 {
			refname = fspec.Name
		}

		if newVal, ok := values[refname]; ok {
			safeSet(&field, newVal)
		}
	}

	return
}

func safeSet(value *reflect.Value, newValue interface{}) {

	switch value.Kind().String() {
	case "int":
		fallthrough
	case "int8":
		fallthrough
	case "int16":
		fallthrough
	case "int32":
		fallthrough
	case "int64":
		value.SetInt(reflect.ValueOf(newValue).Int())
	case "string":

		if v, ok := newValue.(string); ok {
			value.SetString(v)
		} else if v, ok := newValue.([]rune); ok {
			value.SetString(string(v))
		} else if v, ok := newValue.([]uint8); ok {
			value.SetString(string(v))
		}
	case "bool":

		if v, ok := newValue.(bool); ok {
			value.SetBool(v)
		} else if v, ok := newValue.(int64); ok {
			value.SetBool(v != 0)
		} else if v, ok := newValue.(int32); ok {
			value.SetBool(v != 0)
		} else if v, ok := newValue.(int16); ok {
			value.SetBool(v != 0)
		} else if v, ok := newValue.(int8); ok {
			value.SetBool(v != 0)
		} else if v, ok := newValue.(int); ok {
			value.SetBool(v != 0)
		}

	case "struct":

		if value.Type().String() == "time.Time" {

			var tval time.Time
			var terr error

			if v, ok := newValue.(time.Time); ok {
				tval = v
			} else if v, ok := newValue.(string); ok {
				tval, terr = time.Parse("2006-01-02 15:04:05", v)
			} else if v, ok := newValue.([]rune); ok {
				tval, terr = time.Parse("2006-01-02 15:04:05", string(v))
			} else if v, ok := newValue.([]uint8); ok {
				tval, terr = time.Parse("2006-01-02 15:04:05", string(v))
			} else {
				terr = errors.New("Invalid Type")
			}

			if terr == nil {
				value.Set(reflect.ValueOf(tval))
			}
		}

	default:

		v := reflect.ValueOf(newValue)

		if value.CanSet() && value.Kind() == v.Kind() {
			value.Set(v)
		}
	}
}