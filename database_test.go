package sql

import (
	"testing"
	"reflect"
	"time"
	"os"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

type Person struct {
	ID int64 `db:"id"`
	Name string `db:"name"`
	Height float64 `db:"height"`
	Born time.Time `db:"born"`
	IsAlive bool `db:"is_alive"`
}

const TestDatabase = "test.sqlite3"
const TestTable = "people"
var db *DB

var TestData = []Person{
	Person{1, "Person 1", 1.6, getTime("1985-01-01"), true},
	Person{2, "Person 2", 1.7, getTime("1985-02-01"), true},
	Person{3, "Person 3", 1.8, getTime("1985-03-01"), true},
	Person{4, "Person 2", 1.7, getTime("1855-01-01"), false},
}

func init() {
	os.Remove(TestDatabase)
}

func AssertEqual(val1 interface{}, val2 interface{}) bool {

	v1 := reflect.ValueOf(val1)
	v2 := reflect.ValueOf(val2)

	if v1.Kind() == reflect.Ptr {
		v1 = v1.Elem()
	}

	if v2.Kind() == reflect.Ptr {
		v2 = v2.Elem()
	}

	return (v1.Type() == v2.Type() && (v1.Interface() == v2.Interface()))
}

func openDatabase(t *testing.T) (db *DB, err error) {
	
	db = &DB {
		Driver: "sqlite3",
		Name: TestDatabase,
	}

	err = db.Open()

	return
}

func getTime(timeAsString string) (result time.Time) {
	
	result, _ = time.Parse("2006-01-02 15:04:05", timeAsString)
	
	return
}

func TestCreateTable(t *testing.T) {

	db, err := openDatabase(t)

	if err != nil {
		t.Error(err)
		return
	}
	defer db.Close()
	
	createQuery := fmt.Sprintf("CREATE TABLE %s (id bigint not null primary key, name varchar(128), height numeric(12), born date, is_alive boolean)", TestTable)

	_, err = db.Exec(createQuery)

	if err != nil {
		t.Error(err)
	}
}

func TestInsert(t *testing.T) {

	db, err := openDatabase(t)

	if err != nil {
		t.Error(err)
		return
	}
	defer db.Close()

	for _, person := range TestData {

		var res Result

		res, err = db.Insert(TestTable, person)

		if err != nil {
			t.Error(err)
			break
		}

		ra, err := res.RowsAffected()

		if  err != nil {
			t.Error(err)
			break
		}

		if ra != 1 {
			t.Error(fmt.Errorf("failed to insert %v\n", person))
			break
		}
	}
}

func TestCount(t *testing.T) {
	
	db, err := openDatabase(t)

	if err != nil {
		t.Error(err)
		return
	}
	defer db.Close()

	count, err := db.Count(TestTable, "")

	if err != nil {
		t.Error(err)
		return
	}

	if !AssertEqual(count, int64(len(TestData))) {
		t.Error(fmt.Errorf("TestCount() expected %d != %d", count, len(TestData)))
	}

	count, err = db.Count(TestTable, "is_alive = ?", true)

	if err != nil {
		t.Error(err)
		return
	}

	if !AssertEqual(count, int64(3)) {
		t.Error(fmt.Errorf("TestCount() expected %d != %d", count, 3))
	}
}

func TestSelect(t *testing.T) {

	db, err := openDatabase(t)

	if err != nil {
		t.Error(err)
		return
	}
	defer db.Close()

	people := []Person{}

	err = db.Select(&people, fmt.Sprintf("select * from %s", TestTable))

	if err != nil {
		t.Error(err)
		return
	}

	if len(people) != 4 {
		t.Error(fmt.Errorf("TestSelect() => expected 4 p"))
	}
}

func TestGet(t *testing.T) {

	db, err := openDatabase(t)

	if err != nil {
		t.Error(err)
		return
	}
	defer db.Close()

	person := Person{}

	err = db.Get(&person, fmt.Sprintf("select * from %s where id = ?", TestTable), 1)

	if err != nil {
		t.Error(err)
		return
	}

	if !AssertEqual(person.Name, TestData[0].Name) {
		t.Error(fmt.Errorf("TestGet() person.Name != \"%s", TestData[0].Name))
	}

	if !AssertEqual(person.Height, TestData[0].Height) {
		t.Error(fmt.Errorf("TestGet() person.Height != %f\n", TestData[0].Height))
	}
}

func TestDone(t *testing.T) {
	os.Remove(TestDatabase)
}