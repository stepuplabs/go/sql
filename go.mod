module gitlab.com/stepuplabs/go/sql

go 1.13

require (
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	gitlab.com/stepuplabs/go/config v1.0.5
)
