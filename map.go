package sql

import (
	"log"
	"reflect"
)

type FieldMap struct {
	fields map[string]interface{}
}

func NewFieldMap(object interface{}) (fieldMap *FieldMap) {
	
	val := reflect.ValueOf(object)

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	typ := val.Type()

	for i:= 0; i < typ.NumField(); i++ {
		structField := typ.Field(i)
		valueField := val.Field(i)

		log.Printf("[%d] %s = %v\n", i, structField.Name, valueField.Interface())
	}

	return nil
}